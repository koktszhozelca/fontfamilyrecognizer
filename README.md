# Font Family Recognizer

## Getting started
```bash
# Install the libraries, make sure to use python3 
python3 -m virtualenv venv
source venv/bin/activate
pip install -r requirement.txt

python index.py
```