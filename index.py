from app.data import Data
from app.utils.images import ImageHandler
from app.neural_network import NeuralNetwork
from app.web.api_server import start_api_server
from app.config import AVAIL_FONTS


def build_model():
    for font in AVAIL_FONTS:
        Data.process(f'images/{font}.png', 2, 5, font)

    in_data, out_data, labels = Data.get_multiple_training_data(
        AVAIL_FONTS, 2, 5,)

    model = NeuralNetwork.get_instance(ignore_save=True)
    model = NeuralNetwork.train_model(model, in_data, out_data, iteration=1000)
    NeuralNetwork.save_model(model)


def predict():
    in_data, out_data, labels = Data.get_multiple_training_data(
        AVAIL_FONTS, 2, 5,)
    model = NeuralNetwork.get_instance(ignore_save=False)
    import torch

    def test(font, num, noise):
        mat, label = Data.get_target_training_data(
            font, num, noise_levels=[noise])
        data = torch.tensor(mat)
        prediction = model(data)
        val = prediction.detach().numpy()[0]
        return round(val[0]), round(val[1])

    num_index, font_index = test('roboto', 0, 'less')
    num, font = labels[num_index].split("_")[1], AVAIL_FONTS[font_index]
    print(
        f"Num: {num}, Font: {font}, Num Match: {int(num) == 0}, Font Match: {font == 'roboto'}")

    num_index, font_index = test('roboto', 1, 'less')
    num, font = labels[num_index].split("_")[1], AVAIL_FONTS[font_index]
    print(
        f"Num: {num}, Font: {font}, Num Match: {int(num) == 1}, Font Match: {font == 'roboto'}")

    num_index, font_index = test('recursive', 0, 'less')
    num, font = labels[num_index].split("_")[1], AVAIL_FONTS[font_index]
    print(
        f"Num: {num}, Font: {font}, Num Match: {int(num) == 0}, Font Match: {font == 'recursive'}")

    num_index, font_index = test('recursive', 1, 'less')
    num, font = labels[num_index].split("_")[1], AVAIL_FONTS[font_index]
    print(
        f"Num: {num}, Font: {font}, Num Match: {int(num) == 1}, Font Match: {font == 'recursive'}")

    num_index, font_index = test('sora', 0, 'less')
    num, font = labels[num_index].split("_")[1], AVAIL_FONTS[font_index]
    print(
        f"Num: {num}, Font: {font}, Num Match: {int(num) == 0}, Font Match: {font == 'sora'}")

    num_index, font_index = test('sora', 1, 'less')
    num, font = labels[num_index].split("_")[1], AVAIL_FONTS[font_index]
    print(
        f"Num: {num}, Font: {font}, Num Match: {int(num) == 1}, Font Match: {font == 'sora'}")


build_model()
predict()
# start_api_server()
