import json
from aiohttp import web
from base64 import b64decode
from app.utils.images import ImageHandler
from app.neural_network import NeuralNetwork
from app.data import Data
from app.config import AVAIL_FONTS

async def get(request):
    return web.FileResponse("app/web/predict.html")

async def file_handler(request):
    model = NeuralNetwork.get_instance(ignore_save=False)
    labels = Data.get_training_labels(
        AVAIL_FONTS, 2, 5,
        # factor={'roboto': 0, 'recursive': 40, 'sora': 80, 'crimson': 120}
    )
    data_url = (await request.json())['image']
    header, encoded = data_url.split(",",1)
    data = b64decode(encoded)
    with open("app/web/temp/tmp.png", 'wb') as f:
        f.write(data)
    img = Data.convert_testing_data("app/web/temp/tmp.png")
    prediction = model(img).detach().numpy()
    [lbl_index, font_index] = prediction[0]
    lbl_index = round(lbl_index)
    font_index = round(font_index)
    num = labels[lbl_index].split("_")[1]
    print(f"[Prediction] font: {AVAIL_FONTS[font_index]}, Number: {num}")
    return web.Response(body=json.dumps({'font': AVAIL_FONTS[font_index], 'num': num}), content_type='application/json')

def start_api_server():
    app = web.Application()
    app.add_routes([
        web.get("/", get),
        web.post("/file-upload", file_handler),
    ])
    web.run_app(app)
