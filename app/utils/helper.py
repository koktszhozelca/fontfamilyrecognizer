from PIL import Image
import torchvision.transforms as Transform

transform = Transform.Compose([
    Transform.Resize((30, 30), interpolation=Image.NEAREST),
    Transform.ToTensor(),
    Transform.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225]),
])