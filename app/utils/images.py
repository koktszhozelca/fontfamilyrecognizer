import cv2
import numpy as np
import os
from PIL import Image
from app.config import SLOT_HEIGHT, SLOT_WIDTH, NOISE_LEVEL
from app.utils.helper import transform


class ImageHandler:
    @classmethod
    def remove_white(cls, mat):
        gray = cv2.cvtColor(mat, cv2.COLOR_BGR2GRAY)
        th, threshed = cv2.threshold(gray, 240, 255, cv2.THRESH_BINARY_INV)

        # (2) Morph-op to remove noise
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
        morphed = cv2.morphologyEx(threshed, cv2.MORPH_CLOSE, kernel)

        # (3) Find the max-area contour
        cnts = cv2.findContours(
            morphed, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        cnt = sorted(cnts, key=cv2.contourArea)[-1]

        # (4) Crop and save it
        x, y, w, h = cv2.boundingRect(cnt)
        dst = mat[y:y+h, x:x+w]
        return dst

    @classmethod
    def get_transformed_img(cls, img_path):
        img = Image.open(img_path).convert('RGB')
        return transform(img)

    @classmethod
    def get_mat(cls, img_path):
        return cv2.imread(img_path)

    @classmethod
    def get_img_slot(cls, img_path, row, col):
        img = cv2.imread(img_path)
        return img[row * SLOT_HEIGHT: row * SLOT_HEIGHT + SLOT_HEIGHT, col * SLOT_WIDTH: col * SLOT_WIDTH + SLOT_WIDTH]

    @classmethod
    def save(cls, img_path, img_mat):
        cv2.imwrite(img_path, img_mat)

    @classmethod
    def show_img_mat(cls, img_mat, **kwargs):
        cv2.imshow(kwargs.get('name', 'Image Handler')
                   or 'Image Handler', img_mat)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    @classmethod
    def show_img_path(cls, img_path, **kwargs):
        img = cv2.imread(img_path)
        cls.show_img_mat(img, **kwargs)

    @classmethod
    def add_noise(cls, mat, level='less'):
        row, col, ch = mat.shape
        s_vs_p = 0.5
        amount = NOISE_LEVEL[level]
        out = np.copy(mat)
        # Salt mode
        num_salt = np.ceil(amount * mat.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt))
                  for i in mat.shape]
        out[coords] = 1

        # Pepper mode
        num_pepper = np.ceil(amount * mat.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper))
                  for i in mat.shape]
        out[coords] = 0
        return out
