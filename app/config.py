# Model name
MODEL_NAME = "FontFamilyRecognizer"

# Image training data
SLOT_HEIGHT = 200
SLOT_WIDTH = 200

# Image noise level
NOISE_LEVEL = {
    'less': 0.004,
    'medium': 0.04,
    'more': 0.4,
}

# Processed image path
PROCESSED_PATH = "processed"

# Testing image path
TESTING_PATH = "testing"

# Model path
MODEL_PATH = f"models/{MODEL_NAME}"

'''
    Model config
    - N is batch size; D_in is input dimension;
    - H is hidden dimension; D_out is output dimension.
'''
MODEL_CONFIG = {
    'batch_size': 64,
    'input_layer_size': 2700,
    'hidden_layer_size': 5000,
    'output_layer_size': 2,
    'learning_rate': 1e-4,
}

AVAIL_FONTS = ['roboto', 'sora', 'recursive', 'crimson']