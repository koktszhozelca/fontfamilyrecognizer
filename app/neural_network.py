import torch
import logging
from os.path import exists as is_file_exists
from app.config import MODEL_PATH, MODEL_CONFIG


class NeuralNetwork:
    @classmethod
    def __create_model(cls, input_layer_size, hidden_layer_size, output_layer_size, **kwargs):
        return torch.nn.Sequential(
            torch.nn.Linear(input_layer_size, hidden_layer_size),
            torch.nn.ReLU(),
            torch.nn.ReLU(),
            torch.nn.ReLU(),
            torch.nn.Linear(hidden_layer_size, output_layer_size),
        )

    @classmethod
    def train_model(cls, model, input_data_set, output_data_set, **kwargs):
        loss_fn = torch.nn.MSELoss(reduction='mean')
        optimizer = torch.optim.Adam(
            model.parameters(), lr=MODEL_CONFIG['learning_rate'])
        iteration = kwargs.get('iteration') or 500
        for t in range(iteration):
            y_pred = model(input_data_set)
            loss = loss_fn(y_pred, output_data_set)
            if not t % 10:
                print(f"{t}, {loss.item()}")
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        return model

    @classmethod
    def save_model(cls, model):
        torch.save(model.state_dict(), MODEL_PATH)

    @classmethod
    def get_instance(cls, ignore_save=False):
        model = cls.__create_model(**MODEL_CONFIG)
        if not ignore_save and is_file_exists(MODEL_PATH):
            snapshot = torch.load(MODEL_PATH)
            model.load_state_dict(snapshot)
            model.eval()
        return model
