import os
import torch
import numpy as np
from app.utils.images import ImageHandler
from app.config import PROCESSED_PATH, TESTING_PATH, AVAIL_FONTS


class Data:
    @classmethod
    def process(cls, img_path, num_rows, num_cols, postfix, **kwargs):
        for row in range(num_rows):
            for col in range(num_cols):
                mat = ImageHandler.get_img_slot(img_path, row, col)
                mat = ImageHandler.remove_white(mat)

                ImageHandler.save(
                    f"{TESTING_PATH}/num_{row * num_cols + col}_{postfix}.png", mat)

                ImageHandler.save(
                    f"{PROCESSED_PATH}/num_{row * num_cols + col}_{postfix}_none.png", mat)

                mat = ImageHandler.add_noise(mat, level='less')
                ImageHandler.save(
                    f"{PROCESSED_PATH}/num_{row * num_cols + col}_{postfix}_less.png", mat)

                mat = ImageHandler.add_noise(mat, level='medium')
                ImageHandler.save(
                    f"{PROCESSED_PATH}/num_{row * num_cols + col}_{postfix}_medium.png", mat)

                mat = ImageHandler.add_noise(mat, level='more')
                ImageHandler.save(
                    f"{PROCESSED_PATH}/num_{row * num_cols + col}_{postfix}_more.png", mat)

    @classmethod
    def get_training_labels(cls, postfixs, num_rows, num_cols, **kwargs):
        labels = []
        for postfix in postfixs:
            i, o, l = cls.get_training_data(
                postfix, num_rows, num_cols, **kwargs)
            labels += l
        return labels

    @classmethod
    def get_multiple_training_data(cls, postfixs, num_rows, num_cols, **kwargs):
        in_data = []
        out_data = []
        labels = []
        kwargs['no_tensorify'] = True
        for postfix in postfixs:
            i, o, l = cls.get_training_data(
                postfix, num_rows, num_cols, **kwargs)
            in_data += i
            out_data += o
            labels += l
        in_data = torch.tensor(in_data)
        out_data = torch.tensor(out_data, dtype=torch.float)
        return in_data, out_data, labels

    @classmethod
    def get_training_data(cls, postfix, num_rows, num_cols, **kwargs):
        input_data = []
        labels = []
        for row in range(num_rows):
            for col in range(num_cols):
                target_num = row * num_cols + col
                mats, lbls = cls.get_target_training_data(
                    postfix, target_num, **kwargs)
                input_data += mats
                labels += lbls
        factor = kwargs['factor'][postfix] or 1 if kwargs.get('factor') else 1
        if not kwargs.get('no_tensorify'):
            input_data = torch.tensor(input_data)
            output_data = torch.tensor([[index + factor, AVAIL_FONTS.index(postfix)] for index in range(len(labels))], dtype=torch.float)
        else:
            output_data = [[index + factor, AVAIL_FONTS.index(postfix)] for index in range(len(labels))]
        return input_data, output_data, labels

    @classmethod
    def get_target_training_data(cls, postfix, target_num, **kwargs):
        mats = []
        labels = []
        for noise in kwargs.get('noise_levels') or ['none', 'less', 'medium', 'more']:
            path = f"{PROCESSED_PATH}/num_{target_num}_{postfix}_{noise}.png"
            img = ImageHandler.get_transformed_img(path)
            img = torch.flatten(img).numpy()
            mats.append(img)
            labels.append(f"{postfix}_{target_num}_{noise}")
        return mats, labels

    @classmethod
    def get_target_testing_data(cls, font, row, col):
        path = f"images/{font}.png"
        img = ImageHandler.get_img_slot(path, row, col)
        img = ImageHandler.remove_white(img)
        return img

    @classmethod
    def convert_testing_data(cls, path):
        img = ImageHandler.get_transformed_img(path)
        img = torch.flatten(img).numpy()
        return torch.tensor([img] * 30)
